console.log('Starting notes...');
var fs=require('fs');

//since the fetching of code will be required by all the operations its adds on the reusiblity so btr to make it as seprate function that willl be called more than onces
var fetchNotes=()=>{
  //here i fetching the notes fron .json
  try{
    //here i am going to read the data to the existing array in .json
    var noteString=fs.readFileSync('notes-data.json');
    return JSON.parse(noteString);
  }catch(e){
    //if no data found than
    return [];
  }
};
//below function is to save the new note
var saveNotes =(notes) =>{
//only writing of info in array will happen
fs.writeFileSync('notes-data.json',JSON.stringify(notes));
};
var addNote=(title,body)=>{
  // console.log('Adding title',title,body);
/*here, i am trying to get all the data present in .json and append the new one else jus append*/
  var notes=fetchNotes();
//this is input array which i am going to take  the input for
  var note={
    title,
    body
  };

//inorder to check the array if any dulplicates occurs in the array than that has be shown
//1.finding the duplicates of the array & storing it in duplicatearr
var duplicateNotes=notes.filter((note)=>{
  return note.title ===  title;
});
//if length of the duplicate array is ==0
if(duplicateNotes.length === 0)
{
  //push function is going to append the array at end of next
  notes.push(note);
  saveNotes(notes);
  //since the call to this operation function has come from app.js so the result has to printed back to the app.js from where the function call has come from
  return note;
}

 };
var getAll=()=>{
  //goal-> use to grab all notes and send them to app.js to print them for user
  // console.log("Getting all notes");
  return fetchNotes();
};
var deleteNote=(title)=>{
//  console.log('deleting the note');
/*goal
1.fetch the noteString
2.filter notes,removing the one with title of argument
3.save the notes array
*/
var notes=fetchNotes();
var filteredNote=notes.filter((note)=>{
  return note.title !== title;
});
saveNotes(filteredNote);
//now to print if note was removed or not following logic is applied
return notes.length !== filteredNote.length;
};
var readNote= (title) =>{
  //console.log('reading the note',title);
   var rnotes=fetchNotes();
  // var filterRnotes=rnotes.filter((note)=>{
  //   // if(note.title === title)
  //   // {
  //   //   return filterRnotes[0];
  //   // }else{
  //   //   return false;
  //   // }
  //
  // });
  var filterRnotes=rnotes.filter((note)=>note.title === title);
  return filterRnotes[0];

};
var logNote=(note)=>{
  debugger;
  console.log(`Title: ${note.title}`);
  console.log(`Body: ${note.body}`);
  console.log("---");
};

module.exports={
  //here the property whoes value is same as property than use es6 menthod of declaration
   addNote,
   getAll,
   readNote,
   deleteNote,
   logNote
 };
