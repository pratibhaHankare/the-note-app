console.log('Starting the app..');

//loading in bulid module of nodeJS
const fs=require('fs');
//loading the third party module
const _=require('lodash');
const yargs= require('yargs');
//loading custom module
const notes=require('./notes.js');
//creating reusble varible
const titleOptions={
  describe:'Title of note',
  demand:true,
  alias:'t'
};
const bodyOptions={
    describe:'The body of the note',
    demand:true,
    alias:'b'
};

const argv=yargs
          .command('add','Add a new note',{
            title:titleOptions,
            body:bodyOptions
          })
          .command('list','list all notes')
          .command('read','Read a more',{
            title:titleOptions
          })
          .command('delete','Removing the note',{
            title:titleOptions
          })
          .help()
          .argv;

//here argv-> arrugument vector more likly its a arugment array.
//console.log(process.argv);
//i am taking only argv[2] coz its list where list is taken
//this how we grab the value of [0] nd at yarg we have name of are command stored
 var command=argv._[0];
//var command=process.argv[2];
//o/p only the thats present in command
console.log('commands:',command);
//o/p the original procrss.argv array
// console.log('Process:',process.argv);
//o/p the argument list of argv
//console.log('Yargs:',argv);
if(command == "add"){
  // console.log("Adding new note");
  var note=notes.addNote(argv.title,argv.body);
  if(note){
    console.log('Note Created:');
    //es6 was of printing the obj
    notes.logNote(note);
    // console.log("The note is sucessfully added  with title:-"+note.title);
  }else{
    console.log("---");
    console.log(`The note title already exists`);
    console.log("---");

  }
}else if(command == "list"){
  // console.log("listing the notes");
  var allNotes=notes.getAll();
  console.log(`Printing ${allNotes.length} note(s).`);
  allNotes.forEach((note)=>{
    notes.logNote(note);
  });
}else if(command == "read")
{
  // console.log("reading the notes")
  var note=notes.readNote(argv.title);
  if(note){
    console.log('Note was found.');
    notes.logNote(note);

  }else{
    console.log("---");
    console.log('Note was not Found.');
    console.log("---");
  }

}else if(command == "delete")
{
  // console.log("deleting the entry");
  var noteremoved=notes.deleteNote(argv.title);
  //following is ternary opertaor condition?true:false
  var message= noteremoved ? 'Note was removed':'Note was not found';
  console.log(message);
}else{
  console.log('command is not defined');
}
