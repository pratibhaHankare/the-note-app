var square=x=>x*x;
console.log(square(9));

//if i am going to use functions inside objects than its nit recommended
//to use the arrow function coz we have no acess to "this" varible in it
//eg is shown below

var user={
  name:'pratibha',
  sayHi:()=>{
    console.log(arguments);
    console.log(`hello I am ${this.name}`);
  },
  sayHiAll () {
    console.log(arguments);
    console.log(`hello I am ${this.name}`);
  }
};

user.sayHiAll(1);
