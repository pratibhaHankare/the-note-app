 // var obj={
//   name:"pratibha"
// };
//this .stringify function is used to take the object and create a string
// var stringObj=JSON.stringify(obj);
// console JSON.log(typeof stringObj);
// console.log(stringObj);
// JSON.parse() -> this function is used to convert string into object/variable form
// var personString='{"name":"pratibha","age":25}';
// var person=JSON.parse(personString);
// console.log(typeof person);
// console.log(person);

const fs=require('fs');
var originalNote={
  title:'some title',
  body:'some body'
};
var originalNoteString=JSON.stringify(originalNote);
//writing this object inthe file
fs.writeFileSync('notes.json',originalNoteString);
var noteString=fs.readFileSync('notes.json');
var note=JSON.parse( noteString);
console.log(typeof note);
console.log(note.title);
